from answer import answer


def start_user_test():
    print('Тестируйте своего бота. Для выхода нажмите ctr+C или введите endtest')
    print('Если бот вывел вам "-", это значит, что ваш алгоритм не знает, что ответить')
    while True:
        message = input('Человек: ')
        if message == 'endtest':
            return
        text_to_answer = answer(message)
        if type(text_to_answer) is list:
            for phrase in text_to_answer:
                print('Бот: ', phrase)
        else:
            if not text_to_answer:
                text_to_answer = '-'
            print('Бот: ', text_to_answer)


if __name__ == '__main__':
    start_user_test()
