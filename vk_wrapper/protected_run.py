import codejail
import codejail.jail_code
import codejail.safe_exec
import sys
import os
from time import sleep


if __name__ == '__main__':
    try:
        codejail.jail_code.configure('python3', '/home/noah/int-sandbox/venv-sandbox/bin/python3')
        f = open(f'{os.path.dirname(os.path.abspath(__file__))}/vk_wrapper.py', 'r')
        data = f.read()
        f.close()
        sleep(1)
        codejail.safe_exec.safe_exec(data, {})
    except Exception as e:
        print(e, file=sys.stderr)
