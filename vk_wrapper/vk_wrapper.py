import vk_api
import sys
from vk_api.longpoll import VkLongPoll, VkEventType
from answer import answer, TOKEN
from vk_api.utils import get_random_id


class VkBot:

    def __init__(self, token):
        self.vk_session = vk_api.VkApi(token=token)
        self.error = False
        try:
            self.vk_session.auth()
            self.vk_session._auth_token()
        except vk_api.AuthError as error_msg:
            print(error_msg, file=sys.stderr)
            self.error = True
        self.longpoll = VkLongPoll(self.vk_session)
        self.vk = self.vk_session.get_api()

    def get_updates(self):
        if not self.error:
            count_msg = 0
            print('start listening')
            for event in self.longpoll.listen():
                if event.type == VkEventType.MESSAGE_NEW:
                    if count_msg > 0:
                        count_msg -= 1
                        continue
                    text = answer(event.text)
                    if text:
                        if type(text) is list:
                            for msg in text:
                                self.vk.messages.send(peer_id=event.user_id, message=msg, random_id=get_random_id())
                                count_msg += 1
                        else:
                            self.vk.messages.send(peer_id=event.user_id, message=text, random_id=get_random_id())
                            count_msg += 1
                    else:
                        print('bot_msg is empty', file=sys.stderr)


vk = VkBot(TOKEN)
vk.get_updates()
