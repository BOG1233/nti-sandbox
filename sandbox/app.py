from flask import Flask, request
from flask.json import jsonify
import os
from shutil import copy
import subprocess
from time import sleep

app = Flask(__name__)
processes = {}
main_path = '/codejail-nti/'


@app.route('<command_name>/vkbot/upload/', methods=['POST', 'GET'])
def run_file(command_name, request):
    if request.method == 'POST':
        team_name = command_name
        check_pid_in_dist(team_name)
        file = request.files.get('answer.py')
        if not file:
            return jsonify({'status': 'error',
                            'description': 'file not found',
                            'code': 401})
        path = f'{main_path}/{team_name}/'

        if os.path.exists(path):
            file.save(f'{path}answer.py')
            process = subprocess.Popen(['python3', f'{path}protected_run.py'], stdout=subprocess.PIPE)
            sleep(2)
            # без ошибок ли запущен
            data = process.communicate()
            if not data:
                processes[team_name] = process
            else:
                # выводим ошибку
                for line in data:
                    print(line)

        else:
            os.makedirs(path)
            copy(f'{main_path}vk_wrapper/vk_wrapper.py', path)
            copy(f'{main_path}vk_wrapper/protected_run.py', path)
            file.save(f'{path}answer.py')

        return jsonify({'status': 'ok',
                        'code': 200})


def check_pid_in_dist(team_name):
    if not processes.get(team_name):
        processes[team_name] = None
    else:
        processes[team_name].terminate()
