import subprocess
from time import sleep


def test_start_sandbox():
    path = '/tmp/codejail-nti/kek/'

    process = subprocess.Popen(['python3', f'{path}protected_run.py'], stdout=subprocess.PIPE)
    sleep(2)
    # без ошибок ли запущен
    data = process.communicate()
    if not data:
        print(process)
    else:
        # вводим ошибку
        for line in data:
            print(data)
